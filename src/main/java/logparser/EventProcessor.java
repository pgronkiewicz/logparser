package logparser;

import logparser.db.DBManager;
import logparser.model.Event;
import lombok.extern.slf4j.Slf4j;

/**
 * Class for handling events
 *
 */
@Slf4j
public class EventProcessor {
	
    private DBManager dbManager;
    private EventDurationChecker eventChecker;

    public EventProcessor(EventDurationChecker eventChecker, DBManager dbManager) {
    	this.dbManager = dbManager;
    	this.eventChecker = eventChecker;
    }
    
    public void initDB() {
    	dbManager.startHSQLDB();
    }
    
    public void stopDB() {
    	dbManager.stopHSQLDB();
    }
	
	public void processEvent(Event event, int treshold) {
		event.setAlert(checkEventException(event,treshold));
        dbManager.saveEvent(event);
        log.debug("Saved new event into the DB:{}", event.getStart().getId());
	}
	
	private boolean checkEventException(Event event, int treshold) {
		return eventChecker.isDurationLongerThanTreshold(event, treshold);
	}

}
