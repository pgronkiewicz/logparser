package logparser;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import logparser.db.DBManager;
import logparser.model.Event;
import logparser.model.EventLog;
import logparser.model.EventState;
import lombok.extern.slf4j.Slf4j;

/**
 * Main logic for processing the log file and handling the events
 *
 */
@Slf4j
public class LogProcessor {
	
	private EventProcessor eventProcessor;
	private EventLogMapper eventLogMapper;
	private ParserParameters parserParameters;
	private Map<String, EventLog> eventLogsMap;
    
	LogProcessor(ParserParameters parserParameters) {
		this.parserParameters = parserParameters;
		eventLogsMap = new HashMap<>();
		eventLogMapper = new EventLogMapper();
		eventProcessor = new EventProcessor(new EventDurationChecker(), new DBManager());		
	}
	
    public void processLogEvents() {
    	eventProcessor.initDB();
    	try (LineIterator it = FileUtils.lineIterator(new File(parserParameters.getFilePath()), StandardCharsets.UTF_8.name())) {
            while (it.hasNext()) {
                processEventLogLine(it.nextLine());
            }
            eventProcessor.stopDB();
        } catch (IOException e) {
            log.error("There was an error processing event logs file");
			e.printStackTrace();
		}
    }

    private void processEventLogLine(String eventLogLine) {
        Optional<EventLog> eventLogOptional = eventLogMapper.mapLogsToEvents(eventLogLine);
        eventLogOptional.ifPresent(this::processEventLog);
    }
    
    private void processEventLog(EventLog eventLog) {
        log.debug("Processing new {}", eventLog);

        if (didEventLogAlreadyOccur(eventLog.getId())) {
            processEvent(eventLog);
        } else {
            eventLogsMap.put(eventLog.getId(), eventLog);
        }
    }
    
    private void processEvent(EventLog eventLog) {
    	Event event = acquireProcessableEvent(eventLog);
    	eventProcessor.processEvent(event, parserParameters.getTreshold());
    	eventLogsMap.remove(eventLog.getId());
    }
    
    private Event acquireProcessableEvent(EventLog eventLog) {
    	Event event;
    	if(eventLog.getState().equals(EventState.FINISHED)) {
    		event = new Event(eventLogsMap.get(eventLog.getId()), eventLog);
    	} else {
    		event = new Event(eventLog, eventLogsMap.get(eventLog.getId()));
    	}
    	return event;
    }
    
    private boolean didEventLogAlreadyOccur(String eventId) {
        return eventLogsMap.containsKey(eventId);
    }

}
