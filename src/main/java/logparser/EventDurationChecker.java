package logparser;

import logparser.model.Event;

/**
 * Class to check if alert should be triggered for the event based on a provided treshold
 *
 */
public class EventDurationChecker {
	
	
	/**
	 * Checks if duration of the event is longer than specified duration in seconds
	 * @param event
	 * @param seconds
	 * @return
	 */
	public Boolean isDurationLongerThanTreshold(Event event, long seconds) {
		return event.getFinish().getTimestamp() - event.getStart().getTimestamp() > seconds;
	}

}
