package logparser.exceptions;

import java.io.IOException;

public class EventFileProcessingException extends RuntimeException{
	public EventFileProcessingException(IOException e) {
		super(e);
	}

}
