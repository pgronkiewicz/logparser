package logparser;

/**
 * Class the bundle events into and events map
 *
 */
import java.io.IOException;
import java.util.Optional;

import com.fasterxml.jackson.databind.ObjectMapper;

import logparser.model.EventLog;
import lombok.extern.slf4j.Slf4j;

@Slf4j
class EventLogMapper {

    private ObjectMapper objectMapper;

    public EventLogMapper() {
        this.objectMapper = new ObjectMapper();
    }

    public Optional<EventLog> mapLogsToEvents(String eventLogLine) {
        try {
            return Optional.of(
                    objectMapper.readValue(eventLogLine, EventLog.class)
            );
        } catch (IOException e) {
            log.error("Could not map the event log line: {}", eventLogLine, e);
            return Optional.empty();
        }
    }
}
