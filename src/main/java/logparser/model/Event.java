package logparser.model;

import lombok.Data;

/**
 * Representation of an event that contains start and finish log and alert state
 *
 */
@Data
public class Event {
	
	private final EventLog start;
    private final EventLog finish;
    private boolean alert;

    public Event(EventLog start, EventLog finish) {
        this.start = start;
        this.finish = finish;
    }
    
    public long getDuration() {
    	return finish.getTimestamp() - start.getTimestamp();
    }

	public void setAlert(boolean isAlert) {
		this.alert = isAlert;
	}

}
