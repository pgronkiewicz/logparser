package logparser.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

/**
 * Event log in the file, which makes part of the event
 *
 */
@Value
public class EventLog {
	
    private final String id;
    private final EventState state;
    private final Long timestamp;
    @JsonIgnoreProperties
    private final String hostname;
    @JsonIgnoreProperties
    private final EventType type;

    @JsonCreator
    public EventLog(@JsonProperty("id") String id, @JsonProperty("state") EventState state, @JsonProperty("timestamp") Long timestamp, @JsonProperty("host") String hostname, @JsonProperty("type")EventType type) {
        this.id = id;
        this.state = state;
        this.timestamp = timestamp;
        this.hostname = hostname;
        this.type = type;
    }
    
    public EventLog(String id, EventState state, Long timestamp, String hostname) {
        this(id, state, timestamp, hostname,EventType.UNKNOWN);
    }
    
    public EventLog(String id, EventState state, Long timestamp) {
    	this(id, state, timestamp, "", EventType.UNKNOWN);
    }
    

	public String getId() {
		return id;
	}

	public EventState getState() {
		return state;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public String getHostname() {
		return hostname;
	}

	public EventType getType() {
		return type;
	}

    

}
