package logparser.model;

/**
 * List of possible event types
 *
 */
public enum EventType {
    APPLICATION_LOG,
    UNKNOWN
}
