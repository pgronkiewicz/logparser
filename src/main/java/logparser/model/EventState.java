package logparser.model;

/**
 * List of possible event states
 *
 */
public enum EventState {
    STARTED,
    FINISHED
}
