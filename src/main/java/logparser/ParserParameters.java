package logparser;

import lombok.Value;

/**
 * Input parameters for a program to run
 *
 */
@Value
public class ParserParameters {
	
    private final String filePath;
    private final int treshold;

    private ParserParameters(String filePath, int treshold) {
        this.filePath = filePath;
        this.treshold = treshold;
    }

    public static ParserParameters newInstance(String filePath, int treshold) {
        return new ParserParameters(filePath, treshold);
    }

}
