package logparser;


/**
 * Entry point for the application
 *
 */
public class Main {
	
    public static void main(String... vars) {
        new LogProcessor(
                new InputValidator(vars).getParserParameters()
        ).processLogEvents();
    }
}
