package logparser;

import logparser.exceptions.FilePathParameterMissingException;
import lombok.extern.slf4j.Slf4j;

/**
 * Class to validate program startup parameters
 *
 */
@Slf4j
public class InputValidator {
	
	private final static int DEFAULT_TRESHOLD = 4;
    private static final int FILE_PATH_PARAMETER_INDEX = 0;
    private static final int EVENT_TRESHOLD_PARAMETER_INDEX = 1;


	private final String[] vars;

    public InputValidator(String... vars) {
        this.vars = vars.clone();
    }
    
    public ParserParameters getParserParameters() {
        return ParserParameters.newInstance(getFilePath(), getTresholdParameter());
    }
    
    private String getFilePath() {
        try {
            return vars[FILE_PATH_PARAMETER_INDEX];
        } catch (ArrayIndexOutOfBoundsException e) {
            log.error("File path parameter is missing");
            throw new FilePathParameterMissingException(e);
        }
    }
    
    private int getTresholdParameter() {
        try {
            return Integer.valueOf(vars[EVENT_TRESHOLD_PARAMETER_INDEX]);
        } catch (NumberFormatException e) {
            log.info("{} should be a number", vars[EVENT_TRESHOLD_PARAMETER_INDEX]);
            log.info("Using default treshold: {}", DEFAULT_TRESHOLD);
            return DEFAULT_TRESHOLD;
        } catch (ArrayIndexOutOfBoundsException e) {
            log.info("No value specified, using default treshold: {}", DEFAULT_TRESHOLD);
            return DEFAULT_TRESHOLD;
        }
    }
	
}
