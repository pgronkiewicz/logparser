package logparser.db;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import lombok.extern.slf4j.Slf4j;

import logparser.model.Event;

import org.hsqldb.Server;

/**
 * Database managed to handle startup, stop of the dabatanse and all operations processed within.
 *
 */
@Slf4j
public class DBManager {
	
	private static final String DATABASE_NAME = "eventsDB";
	private static final String DATABASE_PATH = "file:events";
	private static final String DATABASE_HOSTNAME = "localhost";
	private static final Integer DATABASE_PORT = 3333;
	//TODO: replace with a configurable variable
	private static final String DATABASE_ALERT_TABLE = "EVENTS";
	private static final String DATABASE_EVENT_TABLE_SQL = "CREATE TABLE IF NOT EXISTS "+DATABASE_ALERT_TABLE+"(id INTEGER IDENTITY PRIMARY KEY, logID VARCHAR(50) NOT NULL, duration BIGINT, host VARCHAR(50), type VARCHAR(50), alert BOOLEAN DEFAULT TRUE NOT NULL)";
	private Server hsqlServer = new Server();
	private Connection connection;

    
	public void startHSQLDB() {
		
		hsqlServer.setLogWriter(null);
		hsqlServer.setSilent(true);

		hsqlServer.setDatabaseName(0, DATABASE_NAME);
		hsqlServer.setPort(DATABASE_PORT);
		hsqlServer.setDatabasePath(0, DATABASE_PATH);

		Logger.getLogger("hsqldb.db").setLevel(Level.WARNING);
		System.setProperty("hsqldb.reconfig_logging", "false");
		hsqlServer.start();
		log.info("HSQL Server started");
		createEventsTable();
	}
    
	public void stopHSQLDB() {
		
		if (connection != null) {
			try {
				connection.close();
				log.info("Connection to HSQL Server closed");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		hsqlServer.stop();
		log.info("HSQL Server stopped");
	}
	
    
	private Connection openConnection() {	
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			return DriverManager.getConnection("jdbc:hsqldb:mem://"+DATABASE_NAME, "SA", "");
		} catch (SQLException | ClassNotFoundException e) {
			log.error(e.getMessage(), e);
		}
		return null;
	}
	
	
	public void dropHSQLDBLTable() {
		Statement stmt = null;
		try {
			if (connection == null || connection.isClosed()) {
				connection = openConnection();
			}
			stmt = connection.createStatement();
			stmt.execute("DROP TABLE IF EXISTS "+DATABASE_ALERT_TABLE);
			stmt.close();
			connection.close();
		}  catch (SQLException e) {
			log.error(e.getMessage(), e);
		} 
	}
	
	public void createEventsTable() {
		Statement stmt = null;
		try {
			if (connection == null || connection.isClosed()) {
				connection = openConnection();
			}

			//Check first if table exists
			DatabaseMetaData dbm = connection.getMetaData();
			ResultSet tables = dbm.getTables(null, null, DATABASE_ALERT_TABLE, new String[] {"TABLE"});
			
			if (!tables.next()) {
				stmt = connection.createStatement();
				stmt.executeUpdate(DATABASE_EVENT_TABLE_SQL);
				log.info("Table "+DATABASE_ALERT_TABLE+" created");
				stmt.close();
			}
			tables.close();
			connection.close();

		}  catch (SQLException  e) {
			log.error(e.getMessage(), e);
		} 
	}
	
	//TODO: write separate statement for inserting single events, use insertEvents to do batch insertions only
	public void saveEvent(Event event) {
		ArrayList<Event> events = new ArrayList<>();
		events.add(event);
		insertEvents(events);
	}
	
	
	public void insertEvents(List<Event> eventList) {
		Statement stmt = null; 
		int insertedRows = 0;

		try {
			if (connection == null || connection.isClosed()) {
				connection = openConnection();
			}
			stmt = connection.createStatement(); 
			
			for (Event e : eventList) {

				//Prevent insertion if ID already exists:
				ResultSet existingRow = stmt.executeQuery("SELECT * FROM "+DATABASE_ALERT_TABLE+" WHERE logID='"+e.getStart().getId()+"'");

				if (!existingRow.next()) {

					String updateStatement = "INSERT INTO "+DATABASE_ALERT_TABLE+" (logID, duration, host, type, alert) VALUES('"+e.getStart().getId()+"',"+e.getDuration()+",'"+e.getStart().getHostname()+"','"+e.getStart().getType()+"','"+e.isAlert()+"')";
					insertedRows += stmt.executeUpdate(updateStatement);
				} else {
					log.warn("Event not saved into database. Duplicate Event ID:" +e.getStart().getId());
				}

			}
			connection.commit(); 
			stmt.close();
			connection.close();
		} catch (SQLException e) {
			log.error( e.getMessage(), e);
		} 
		log.debug(insertedRows+" rows inserted in the db");
	}
	
	//TODO: pagination
	public String readEvents() {
		StringBuilder sb  = new StringBuilder();
		Statement stmt = null; 
		ResultSet result;

		try {

			if (connection == null || connection.isClosed()) {
				connection = openConnection();
			}
			
			stmt = connection.createStatement();
			result = stmt.executeQuery("SELECT * FROM "+DATABASE_ALERT_TABLE);
			ResultSetMetaData rsmd = result.getMetaData();
			

			while (result.next()) {
			    for (int i = 1; i <= 5; i++) {
			        if (i > 1) {
			        	sb.append(",  ");
			        }
			        String columnValue = result.getString(i);
			        sb.append(rsmd.getColumnName(i)+":"+columnValue);
			    }
			    sb.append("\n");
			}
			stmt.close();
			result.close();
			connection.close();
		} catch (SQLException e) {
			log.error(e.getMessage(), e);
		}
		return sb.toString();

	}
	

}
