package logparser;

import logparser.EventDurationChecker;
import logparser.model.Event;
import logparser.model.EventLog;
import logparser.model.EventState;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class EventDurationCheckerTest {
	
	@Test
	public void checkLongerDuration() {
		assertTrue(
				new EventDurationChecker().isDurationLongerThanTreshold(
						new Event(
                                new EventLog("scsmbstgrc", EventState.STARTED, 1491377495210L),
                                new EventLog("scsmbstgrc", EventState.FINISHED, 1491377495218L)
                                ), 4L));
		
	}
	
	/*
	 * @Test public void checkEqualDuration() { assertFalse( new
	 * EventDurationChecker().isDurationLongerThanTreshold( new Event( new
	 * EventLog("scsmbstgrb", EventState.START, 1491377495213L), new
	 * EventLog("scsmbstgrb", EventState.FINISH, 1491377495217L) ), 4L));
	 * 
	 * }
	 * 
	 * @Test public void checkShorterDuration() { assertFalse( new
	 * EventDurationChecker().isDurationLongerThanTreshold( new Event( new
	 * EventLog("scsmbstgra", EventState.START, 1491377495255L), new
	 * EventLog("scsmbstgra", EventState.FINISH, 1491377495256L) ), 4L));
	 * 
	 * }
	 */
}
