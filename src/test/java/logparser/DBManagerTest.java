package logparser;

import static org.junit.Assert.assertEquals;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import logparser.db.DBManager;
import logparser.model.Event;
import logparser.model.EventLog;
import logparser.model.EventState;
import logparser.model.EventType;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DBManagerTest {
	
	@Before
	public void cleanupDB() {
		DBManager dbmanager = new DBManager();
		dbmanager.stopHSQLDB();
		dbmanager.startHSQLDB();
		dbmanager.dropHSQLDBLTable();
		dbmanager.stopHSQLDB();
	}
	
	@Test
	public void checkInsertionEventLackingFields() {
		
		String expectedOutput="ID:0,  LOGID:scsmbstgra,  DURATION:5,  HOST:,  TYPE:UNKNOWN\n";
		
		DBManager dbmanager = new DBManager();
		dbmanager.startHSQLDB();
		dbmanager.dropHSQLDBLTable();
		dbmanager.createEventsTable();
		Event event1 = new Event(new EventLog("scsmbstgra", EventState.STARTED, 1491377495212L), new EventLog("scsmbstgra", EventState.FINISHED, 1491377495217L));
		List<Event> list = new ArrayList<>();
		list.add(event1);
		dbmanager.insertEvents(list);
		String result = dbmanager.readEvents();
		log.debug(result);
		assertEquals(result, expectedOutput);
		dbmanager.stopHSQLDB();
	}
	
	@Test
	public void checkInsertionEventAllFields() {
		
		String expectedOutput="ID:0,  LOGID:scsmbstgra,  DURATION:5,  HOST:12345,  TYPE:APPLICATION_LOG\n";
		
		DBManager dbmanager = new DBManager();
		dbmanager.startHSQLDB();
		dbmanager.dropHSQLDBLTable();
		dbmanager.createEventsTable();
		Event event1 = new Event(new EventLog("scsmbstgra", EventState.STARTED, 1491377495212L, "12345", EventType.APPLICATION_LOG), new EventLog("scsmbstgra", EventState.FINISHED, 1491377495217L, "12345", EventType.APPLICATION_LOG));
		List<Event> list = new ArrayList<>();
		list.add(event1);
		dbmanager.insertEvents(list);
		String result = dbmanager.readEvents();
		log.debug(result);
		assertEquals(result, expectedOutput);
		dbmanager.stopHSQLDB();
	}
	
	@Test
	public void checkInsertionDuplicateEvent() {
		
		String expectedOutput="ID:0,  LOGID:scsmbstgra,  DURATION:5,  HOST:12345,  TYPE:APPLICATION_LOG\n";

		DBManager dbmanager = new DBManager();
		dbmanager.startHSQLDB();
		dbmanager.dropHSQLDBLTable();
		dbmanager.createEventsTable();
		Event event1 = new Event(new EventLog("scsmbstgra", EventState.STARTED, 1491377495212L, "12345", EventType.APPLICATION_LOG), new EventLog("scsmbstgra", EventState.FINISHED, 1491377495217L, "12345", EventType.APPLICATION_LOG));
		Event event2 = new Event(new EventLog("scsmbstgra", EventState.STARTED, 1491377495212L, "12345", EventType.APPLICATION_LOG), new EventLog("scsmbstgra", EventState.FINISHED, 1491377495217L, "12345", EventType.APPLICATION_LOG));
		List<Event> list = new ArrayList<>();
		list.add(event1);
		list.add(event2);
		dbmanager.insertEvents(list);
		String result = dbmanager.readEvents();
		log.debug(result);
		assertEquals(result, expectedOutput);
		dbmanager.stopHSQLDB();
	}
	
	@Test
	public void checkInsertionMultipleEvents() {
		
		String expectedOutput="ID:0,  LOGID:scsmbstgra,  DURATION:5,  HOST:,  TYPE:UNKNOWN\n" + "ID:1,  LOGID:scsmbstgrb,  DURATION:5,  HOST:12345,  TYPE:APPLICATION_LOG\n";
		
		DBManager dbmanager = new DBManager();
		dbmanager.startHSQLDB();
		dbmanager.dropHSQLDBLTable();
		dbmanager.createEventsTable();
		Event event1 = new Event(new EventLog("scsmbstgra", EventState.STARTED, 1491377495212L), new EventLog("scsmbstgra", EventState.FINISHED, 1491377495217L));
		Event event2 = new Event(new EventLog("scsmbstgrb", EventState.STARTED, 1491377495212L, "12345", EventType.APPLICATION_LOG), new EventLog("scsmbstgrb", EventState.FINISHED, 1491377495217L, "12345", EventType.APPLICATION_LOG));
		List<Event> list = new ArrayList<>();
		list.add(event1);
		list.add(event2);
		dbmanager.insertEvents(list);
		String result = dbmanager.readEvents();
		log.debug(result);
		assertEquals(result, expectedOutput);
		dbmanager.stopHSQLDB();
	}
	
	
	
	@Test
	public void insertAndReadEvents_insertNullList() {
		DBManager dbmanager = new DBManager();
		dbmanager.startHSQLDB();
		dbmanager.dropHSQLDBLTable();
		dbmanager.createEventsTable();
		List<Event> list = new ArrayList<>();
		dbmanager.insertEvents(list);
		String result = dbmanager.readEvents();
		assertEquals(result, "");
		dbmanager.stopHSQLDB();
	}

}

