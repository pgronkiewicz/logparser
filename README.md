# Log Parser

## Prerequisites

You need an up-to-date gradle version installed. This project was developed with version 6.8.2

### Installing

Build and test from root folder:

```
gradle build
```

## Executing the application

To execute the application, please specify two parameters with the gradle run command:
- input file (you can use the file in root directory as a sample)
- alert treshold in seconds (if no parameter is provided, it will use a default of 4)

```
gradle run --args "<path to file> <treshold>"
```

## Code state

This application is not finished and lacks several aspects:
- increased code coverage
- use batch file processing, e.x. Spring Batch to handle big files
- multithreading
- integration tests on bigger data sample